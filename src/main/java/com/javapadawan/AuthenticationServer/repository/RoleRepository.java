package com.javapadawan.AuthenticationServer.repository;

import com.javapadawan.AuthenticationServer.entity.auth.RegistryRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<RegistryRole, Long> {
    List<RegistryRole> findByName(String name);
}
