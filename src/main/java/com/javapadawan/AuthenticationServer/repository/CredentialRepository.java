package com.javapadawan.AuthenticationServer.repository;

import com.javapadawan.AuthenticationServer.entity.auth.Credential;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CredentialRepository extends JpaRepository<Credential, Long> {
    Optional<Credential> findCredentialByEmail(String email);
}
