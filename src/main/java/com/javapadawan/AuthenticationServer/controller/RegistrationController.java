package com.javapadawan.AuthenticationServer.controller;

import com.javapadawan.AuthenticationServer.service.authorization.AuthorizationService;
import com.javapadawan.AuthenticationServer.service.registration.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/registration")
public class RegistrationController {
    private final RegistrationService registrationService;
    private final AuthorizationService authService;

    @Autowired
    public RegistrationController(RegistrationService registrationService,
                                  AuthorizationService authService) {
        this.registrationService = registrationService;
        this.authService = authService;
    }

    @GetMapping("/patient/{email}")
    public void registerPatient(@RequestHeader(name = "Authorization") String token,
                                @PathVariable String email) {
        try {
            authService.validateToken(token);
            registrationService.registerAsPatient(email);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
