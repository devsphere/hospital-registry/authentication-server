package com.javapadawan.AuthenticationServer.controller;

import com.javapadawan.AuthenticationServer.entity.auth.Credential;
import com.javapadawan.AuthenticationServer.entity.payload.AuthDto;
import com.javapadawan.AuthenticationServer.service.authentication.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/authentication")
public class AuthenticationController {
    private final AuthenticationService service;

    @Autowired
    public AuthenticationController(AuthenticationService service) {
        this.service = service;
    }

    @PostMapping("/credentials")
    public ResponseEntity<AuthDto> authenticate(@RequestBody Credential credential) {
        try {
            Credential authCredentials = service.authenticate(credential);
            String token = service.generateTokenOnSubject(authCredentials);
            String role = service.getRoleByCredentials(authCredentials);
            HttpHeaders tokenHeader = new HttpHeaders();
            tokenHeader.add("Authorization", "Bearer " + token);
            return new ResponseEntity<>(
                    new AuthDto(authCredentials.getEmail(), role, authCredentials.getPassword()),
                    tokenHeader,
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            return new ResponseEntity<>(
                    new AuthDto(ex.getMessage()),
                    HttpStatus.FORBIDDEN
            );
        }
    }
}
