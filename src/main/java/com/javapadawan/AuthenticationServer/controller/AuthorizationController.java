package com.javapadawan.AuthenticationServer.controller;

import com.javapadawan.AuthenticationServer.entity.payload.AuthDto;
import com.javapadawan.AuthenticationServer.service.authorization.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/authorize")
public class AuthorizationController {
    private final AuthorizationService service;

    @Autowired
    public AuthorizationController(AuthorizationService service) {
        this.service = service;
    }

    @GetMapping("/byToken/{token}")
    public AuthDto authorizeByToken(@PathVariable String token) {
        try {
            return service.authorizeByToken(token);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
