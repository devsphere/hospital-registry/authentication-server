package com.javapadawan.AuthenticationServer.service.authentication;

import com.javapadawan.AuthenticationServer.entity.auth.Credential;
import com.javapadawan.AuthenticationServer.entity.auth.RegistryRole;
import com.javapadawan.AuthenticationServer.repository.CredentialRepository;
import com.javapadawan.AuthenticationServer.service.internal.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthenticationService {
    private final TokenService tokenService;
    private final CredentialRepository repository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationService(TokenService tokenService,
                                 CredentialRepository repository,
                                 BCryptPasswordEncoder passwordEncoder) {
        this.tokenService = tokenService;
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    public Credential authenticate(Credential credential) {
        Optional<Credential> authCredential = repository.findCredentialByEmail(credential.getEmail());
        if (authCredential.isEmpty()) {
            throw new IllegalArgumentException("Not registered email");
        }
        if (passwordEncoder.matches(credential.getPassword(), authCredential.get().getPassword())) {
            return authCredential.get();
        } else {
            throw new IllegalArgumentException("Invalid password");
        }
    }

    public String getRoleByCredentials(Credential credential) {
        return extractRoles(credential.getRoles());
    }

    public String generateTokenOnSubject(Credential credential) {
        return tokenService.generateTokenOnSubject(credential.getEmail());
    }

    private String extractRoles(List<RegistryRole> roleList) {
        StringBuilder roles = new StringBuilder();
        for (RegistryRole role : roleList) {
            roles.append(role.getName());
            roles.append(",");
        }
        if (!roles.isEmpty()) {
            roles.deleteCharAt(roles.length() - 1);
        }
        return roles.toString();
    }
}
