package com.javapadawan.AuthenticationServer.service.internal;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;

@Component
public class TokenService {
    private final String jwtSecret;
    private final int jwtExpTimeMs;

    public TokenService(@Value("${app.authentication.jwt-secret}") String jwtSecret,
                        @Value("${app.authentication.jwt-expiration-time}") int jwtExpTimeMs) {
        this.jwtExpTimeMs = jwtExpTimeMs;
        this.jwtSecret = jwtSecret;
    }

    public String getTokenSubject(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public String generateTokenOnSubject(String subject) {
        return Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpTimeMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public boolean isJwtValid(String jwt) {
        if (jwt == null || jwt.isEmpty()) {
            return false;
        }
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwt);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
