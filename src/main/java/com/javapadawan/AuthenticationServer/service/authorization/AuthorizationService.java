package com.javapadawan.AuthenticationServer.service.authorization;

import com.javapadawan.AuthenticationServer.entity.auth.Credential;
import com.javapadawan.AuthenticationServer.entity.auth.RegistryRole;
import com.javapadawan.AuthenticationServer.entity.payload.AuthDto;
import com.javapadawan.AuthenticationServer.repository.CredentialRepository;
import com.javapadawan.AuthenticationServer.service.internal.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorizationService {
    private final CredentialRepository credentialRepo;
    private final TokenService tokenService;

    @Autowired
    public AuthorizationService(CredentialRepository credentialRepo, TokenService tokenService) {
        this.credentialRepo = credentialRepo;
        this.tokenService = tokenService;
    }

    public AuthDto authorizeByToken(String token) {
        if (tokenService.isJwtValid(token)) {
            String email = tokenService.getTokenSubject(token);
            Optional<Credential> credential = credentialRepo.findCredentialByEmail(email);
            if (credential.isEmpty()) {
                throw new IllegalArgumentException("Invalid credentials");
            }
            String role = extractRoles(credential.get().getRoles());
            return new AuthDto(credential.get().getEmail(), role, credential.get().getPassword());
        } else {
            throw new IllegalArgumentException("Invalid token");
        }
    }

    public void validateToken(String token) {
        if (!tokenService.isJwtValid(token)) {
            throw new IllegalArgumentException("Invalid token");
        }
    }

    private String extractRoles(List<RegistryRole> roleList) {
        StringBuilder roles = new StringBuilder();
        for (RegistryRole role : roleList) {
            roles.append(role.getName());
            roles.append(",");
        }
        if (!roles.isEmpty()) {
            roles.deleteCharAt(roles.length() - 1);
        }
        return roles.toString();
    }
}
