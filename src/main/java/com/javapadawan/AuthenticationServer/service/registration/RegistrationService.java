package com.javapadawan.AuthenticationServer.service.registration;

import com.javapadawan.AuthenticationServer.entity.auth.Credential;
import com.javapadawan.AuthenticationServer.entity.auth.RegistryRole;
import com.javapadawan.AuthenticationServer.repository.CredentialRepository;
import com.javapadawan.AuthenticationServer.repository.RoleRepository;
import com.javapadawan.AuthenticationServer.service.internal.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegistrationService {
    private final CredentialRepository credentialRepo;
    private final RoleRepository roleRepo;
    private final PasswordGenerator passwordGenerator;
    private final EmailService emailService;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationService(CredentialRepository credentialRepo,
                               BCryptPasswordEncoder passwordEncoder,
                               PasswordGenerator passwordGenerator,
                               RoleRepository roleRepo,
                               EmailService emailService) {
        this.credentialRepo = credentialRepo;
        this.roleRepo = roleRepo;
        this.passwordGenerator = passwordGenerator;
        this.emailService = emailService;
        this.passwordEncoder = passwordEncoder;
    }

    public void registerAsPatient(String email) {
        Credential credential = new Credential();
        credential.setEmail(email);
        List<RegistryRole> roles = roleRepo.findByName("PATIENT");
        credential.setRoles(roles);
        String rawPassword = passwordGenerator.generatePassword();
        String passwordHash = passwordEncoder.encode(rawPassword);
        credential.setPassword(passwordHash);
        try {
            credentialRepo.save(credential);
            sendNotification(email, rawPassword);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    private void sendNotification(String recipient, String password) {
        String subject = "Loutsker's Hospital registration";
        String text = "Greetings, you have been registered as patient of Loutsker's Hospital!\n" +
                "Account details:\n" +
                "Login - " + recipient + "\n" +
                "Password - " + password + "\n\n" +
                "Please visit our site: http://194.31.174.36:9099\n\n" +
                "Best regards,\n" + "Loutsker's Hospital administration\n" +
                "W Smithfield, London EC1A 7BE, United Kingdom";
        emailService.sendEmail(recipient, subject, text);
    }
}
