package com.javapadawan.AuthenticationServer.service.registration;

import org.apache.commons.text.RandomStringGenerator;
import org.springframework.stereotype.Component;

@Component
public class PasswordGenerator {
    private final RandomStringGenerator generator;
    private final int length;

    public PasswordGenerator() {
        this.generator = new RandomStringGenerator.Builder().withinRange(97, 122).build();
        this.length = 10;
    }

    public String generatePassword() {
        return generator.generate(length);
    }
}
