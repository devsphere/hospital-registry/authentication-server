package com.javapadawan.AuthenticationServer.entity.auth;

import jakarta.persistence.*;

import java.util.*;

@Entity(name = "registry_role")
public class RegistryRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;
    @ManyToMany(mappedBy = "roles")
    private List<Credential> bearers = new ArrayList<>();

    public RegistryRole() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistryRole registryRole = (RegistryRole) o;
        return name.equals(registryRole.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public List<Credential> getBearers() {
        return bearers;
    }

    public void setBearers(List<Credential> bearers) {
        this.bearers = bearers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
