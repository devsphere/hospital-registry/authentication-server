package com.javapadawan.AuthenticationServer;

import com.javapadawan.AuthenticationServer.entity.auth.Credential;
import com.javapadawan.AuthenticationServer.repository.CredentialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@ConfigurationPropertiesScan
public class AuthenticationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationServerApplication.class, args);
	}

//	@Bean
//	@Autowired
//	CommandLineRunner commandLineRunner(CredentialRepository repo, BCryptPasswordEncoder encoder) {
//		return args -> {
//			Credential patientCredentials = new Credential();
//			patientCredentials.setEmail("testtest@gmail.com");
//			patientCredentials.setPassword(encoder.encode("password"));
//			repo.save(patientCredentials);
//		};
//	}
}
