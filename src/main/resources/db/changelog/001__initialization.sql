CREATE TABLE IF NOT EXISTS credential (
    id SERIAL PRIMARY KEY,
    email VARCHAR(255) UNIQUE NOT NULL,
    password_hash varchar(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS registry_role (
    id SERIAL PRIMARY KEY,
    name VARCHAR(22)
);

CREATE TABLE IF NOT EXISTS credential_roles (
    bearer_id INTEGER NOT NULL REFERENCES credential(id)
        ON DELETE CASCADE
        ON UPDATE RESTRICT,
    role_id INTEGER NOT NULL REFERENCES registry_role(id)
        ON DELETE CASCADE
        ON UPDATE RESTRICT
);

INSERT INTO registry_role (name) VALUES ('PATIENT');
INSERT INTO registry_role (name) VALUES ('DOCTOR');
INSERT INTO registry_role (name) VALUES ('ADMIN');
